function ResultScreen(scorePoints, bounds, assetsLoader, scale) {
    let self = this;


    let board = new createjs.Bitmap(assetsLoader.getResult("board"))
        .set({scale: scale});
    let boardBounds = board.getTransformedBounds();
    let container = new createjs.Container()
        .set({
            x: bounds.width/2 - boardBounds.width/2,
            y: bounds.height/2 - boardBounds.height/2,
            width: boardBounds.width,
            height: boardBounds.height,
            cache: true,
        });
    container.addChild(board);
    let boardMessage = new createjs.Text("Total score: " + scorePoints, "bold 32px Arial", "#000")
        .set({
            x: container.getBounds().width/2,
            y: container.getBounds().height/2,
            maxWidth: container.getBounds().width,
            textAlign: "center",
            textBaseline: "middle",
            scale: scale
        });
    container.addChild(boardMessage);

    self.getContainer = function() {
        return container;
    };
}