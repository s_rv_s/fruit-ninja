"use strict";

function GameScreen(bounds, assetsLoader, scale)  {
    let self = this;

    let stage;
    let slimeLayer;
    let fruitsLayer;
    let statusLayer;
    let scorePoints = 0;
    let scorePointsEl;
    let gameTimeEl;
    let gameFullTime = 30000;
    let gameTimeToEnd = gameFullTime/1000;
    let generateFruitsTimerId = null;
    let gameTimeTimerId = null;
    let endGameListener = {
        listener: null,
        context: null
    };
    const fruit_list = [
        {
            id: "fruit_blue",
            points: 30,
            text: "AD\nFATIGUE"
        }, {
            id:"fruit_green",
            points: 5,
            text: "LOW\nROI"
        }, {
            id: "fruit_orange",
            points: 20,
            text: "HI\nCPAS"
        }, {
            id: "fruit_purple",
            points: 15,
            text: "SLOW\nSUPPORT"
        }, {
            id: "fruit_red",
            points: 25,
            text: "BAD\nREPORTING"
        }, {
            id: "fruit_yellow",
            points: 10,
            text: "MANUAL\nWORK"
        }
    ];
    const cutSoundId = "cut_sound";

    let init = function() {
        stage = new createjs.Container()
            .set({
                x: bounds.x, y: bounds.y, width: bounds.width, height: bounds.height
            });
        slimeLayer = new createjs.Container()
            .set({width: bounds.width, height: bounds.height, cache: true });
        stage.addChild(slimeLayer);

        fruitsLayer = new createjs.Container()
            .set({width: bounds.width, height: bounds.height});
        stage.addChild(fruitsLayer);

        statusLayer = new createjs.Container()
            .set({width: bounds.width, height: bounds.height, cache: true });
        stage.addChild(statusLayer);
        initStatuses();

    };

    let startGame = function(){
        window.clearTimeout(generateFruitsTimerId);
        scorePoints = 0;
        slimeLayer.removeAllChildren();
        fruitsLayer.removeAllChildren();
        initStatuses();
        window.setTimeout(endGame, gameFullTime);
        window.clearInterval(gameTimeTimerId);
        gameTimeTimerId = window.setInterval(function() {
            gameTimeToEnd -= 1;
            updateTimeStatus();
            if (gameTimeToEnd <= 0) {
                window.clearInterval(gameTimeTimerId);
            }
        }, 1000);
        generateFruits();
        randomGenerationFruits();
    };

    let endGame = function() {
        window.clearTimeout(generateFruitsTimerId);
        fruitsLayer.removeAllChildren();
        statusLayer.removeAllChildren();

        if(endGameListener.listener) {
            endGameListener.listener.call(endGameListener.context);
        }
    };

    let initStatuses = function() {
        statusLayer.removeAllChildren();
        let scoreMessage = new createjs.Text("Score: ", "bold 24px Arial", "#FFFFFF")
            .set({ x: 30*scale, y: 10*scale, scale: scale});
        statusLayer.addChild(scoreMessage);
        scorePointsEl = new createjs.Text("0", "bold 24px Arial", "#FFFFFF")
            .set({ x: 110*scale, y: 10*scale, scale: scale});
        statusLayer.addChild(scorePointsEl);
        gameTimeEl = new createjs.Text("", "bold 24px Arial", "#FFFFFF")
            .set({ x: bounds.width - 30*scale, y: 10*scale, textAlign: "right", scale: scale});
        updateTimeStatus();
        statusLayer.addChild(gameTimeEl);
    };

    let appendNewFruit = function(fruitId, direction) {
        let container = new createjs.Container();
        let fruit = new createjs.Bitmap(assetsLoader.getResult(fruitId))
            .set({scale: scale});
        let fruitBounds = fruit.getTransformedBounds();

        let deltaX = (bounds.width) * Math.random();
        let deltaY = (bounds.height/4) * Math.random();
        let startX = bounds.width/2 + direction * deltaX;
        let endX = bounds.width/2 - direction * deltaX;
        let middleX = (startX + endX)/2;
        let startY = bounds.height + fruitBounds.height + 2;
        let endY = startY;
        let middleY = deltaY;

        let controlPoint1X = startX;
        let controlPoint1Y = middleY;
        let controlPoint2X = endX;
        let controlPoint2Y = middleY;

        container.set({
            x: startX,
            y: startY,
            width: fruitBounds.width,
            height: fruitBounds.height
        });

        container.addChild(fruit);
        fruitsLayer.addChild(container);

        let cutFruitListener = function(event){
            container.removeEventListener("click", cutFruitListener);
            //container.removeEventListener('pressmove', cutFruitListener);
            onCutFruit(fruit, fruitId, container, event.stageX, event.stageY);
        };
        container.addEventListener("click", cutFruitListener);
        //container.addEventListener('pressmove', cutFruitListener);

        let path = [
            container.x, container.y,

            controlPoint1X, controlPoint1Y,
            middleX, middleY,

            controlPoint2X, controlPoint2Y,
            endX, endY,
        ];
        createjs.Tween.get(container)
            .to({guide:{ path: path }, rotation: direction* 90},7000, createjs.Ease.linear)
            .call(function () {
                fruitsLayer.removeChild(container);
            });
    };

    let generateFruits = function() {
        let size = Math.floor(Math.random() * 2) + 1;
        for(let i=0; i< size; i++) {
            appendNewFruit(getRandomFruitId(), Math.random() > 0.5 ? 1: -1)
        }
    };

    let randomGenerationFruits= function(){
        generateFruitsTimerId = window.setTimeout(function(){
            generateFruits();
            randomGenerationFruits();
        }, Math.random() * 3000);
    };

    let addSlime = function(x, y, fruitId) {
        let fruitSlime = new createjs.Bitmap(assetsLoader.getResult(fruitId + "_s"))
            .set({ x: x, y: y, cache: true, alpha: 0.6, scale: scale});
        let fruitSlimeBounds = fruitSlime.getTransformedBounds();
        fruitSlime.set({
            regX: fruitSlimeBounds.width/2,
            regY: fruitSlimeBounds.height/2
        });
        slimeLayer.addChild(fruitSlime);
    };

    let addSlicedFruit = function(fruitId, fruitContainer) {
        let fruit_l = new createjs.Bitmap(assetsLoader.getResult(fruitId + "_l"))
            .set({scale: scale});
        let fruit_r = new createjs.Bitmap(assetsLoader.getResult(fruitId + "_r"))
            .set({x: fruitContainer.width/4- fruitContainer.width/2*0.1, scale: scale});

        fruitContainer.addChild(fruit_l);
        fruitContainer.addChild(fruit_r);

        createjs.Tween.get(fruit_l).to({
            x: -1* fruitContainer.width/2 * 0.1,
            rotation: 30
        } ,4000, createjs.Ease.cubicOut);

        createjs.Tween.get(fruit_r).to({
            x: fruit_r.x + fruitContainer.width/2 * 0.1,
            rotation: -30
        } ,4000, createjs.Ease.cubicOut);
    };

    let addInfoSlice = function(points, text) {
        let infoEl = new createjs.Text("+" + points + "\n"+ text, "bold 32px Arial", "#FFFFFF")
            .set({
                x: bounds.width/2,
                y: bounds.height/3,
                maxWidth: 100,
                textAlign: "center",
                textBaseline: "middle",
                scale: scale
            });
        slimeLayer.addChild(infoEl);
        createjs.Tween.get(infoEl).to({
            y: -40* scale,
            alpha: 0
        },3000, createjs.Ease.linear).call( function(){
            slimeLayer.removeChild(infoEl);
        });
    };

    let playSound = function(id, loop) {
        createjs.Sound.play(id, {interrupt: createjs.Sound.INTERRUPT_NONE, loop: !!loop, volume: 0.4});
    };

    let onCutFruit = function(fruit, fruitId, container, stageX, stageY) {
        container.removeChild(fruit);
        container.width *= 2;

        playSound(cutSoundId, 0);
        addSlime(stageX, stageY, fruitId);
        addSlicedFruit(fruitId, container);

        let fruitById = getFruitById(fruitId);
        updateScore(fruitById.points);
        addInfoSlice(fruitById.points, fruitById.text);
    };

    let updateScore = function(deltaPoints) {
        deltaPoints && (scorePoints += deltaPoints);
        scorePointsEl.text = "" + scorePoints;
    };

    let updateTimeStatus = function(){
        gameTimeEl.text = "Time left: " + secondsToTime(gameTimeToEnd);
    };

    let getRandomFruitId = function() {
        let fruitsSize = fruit_list.length;
        let idx = Math.floor(Math.random() * Math.floor(fruitsSize));
        return fruit_list[idx].id;
    };

    let getFruitById = function(id) {
        return fruit_list.find(function(fruit){ return fruit.id === id});
    };

    let unPause = function() {
        createjs.Ticker.paused = false;
    };

    let pause = function() {
        createjs.Ticker.paused = true;
    };

    function secondsToTime(secs){
        let minutes = Math.floor(secs / 60);
        let divisor_for_seconds = secs % 60;
        let seconds = Math.ceil(divisor_for_seconds);
        return ((minutes<10 ? "0"+minutes: minutes) + ":" + (seconds<10? "0"+seconds: seconds));
    }

    self.start = function () {
        startGame();
    };

    self.getContainer = function() {
        return stage;
    };

    self.addActionOnGameEnd = function(listener, ctx) {
        endGameListener.listener = listener;
        endGameListener.context = ctx;
    };

    self.getTotalScope = function() {
        return 0 + scorePoints;
    };

    init();
}