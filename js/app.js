'use strict';

function init(elementId) {
    let canvas = document.getElementById("fruitNinjaGameCanvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    let ninjaGame = new ninja(elementId, document.body.clientWidth, document.body.clientHeight);
    ninjaGame.onLoadCompleate(function () {
       document.body.className = document.body.className.replace("loading", "");
    });
    ninjaGame.init();
}

let ninja = function (canvasElementId, width, height) {
    let self = this;
    if(!canvasElementId) {
        throw new Error("Can'n initalize ninja. please specify canvasElementId")
    }

    let stage;
    let scale = 1;
    let onLoadCompleteListener;

    let assetsLoader;
    let assetsManifest = [
        {id: "cut_sound", src:"assets/sword-unsheathing.mp3"},
        {id: "game_bg", src:"assets/game_bg.jpg"},
        {id: "board", src:"assets/board.png"},
        {id: "play_btn", src: "assets/play_btn.png"},
        {id: "fruit_blue", src:"assets/game_fruit_blue.png"},
        {id: "fruit_blue_l", src:"assets/game_fruit_blue_l.png"},
        {id: "fruit_blue_r", src:"assets/game_fruit_blue_r.png"},
        {id: "fruit_blue_s", src:"assets/game_fruit_blue_s.png"},
        {id: "fruit_green", src:"assets/game_fruit_green.png"},
        {id: "fruit_green_l", src:"assets/game_fruit_green_l.png"},
        {id: "fruit_green_r", src:"assets/game_fruit_green_r.png"},
        {id: "fruit_green_s", src:"assets/game_fruit_green_s.png"},
        {id: "fruit_orange", src:"assets/game_fruit_orange.png"},
        {id: "fruit_orange_l", src:"assets/game_fruit_orange_l.png"},
        {id: "fruit_orange_r", src:"assets/game_fruit_orange_r.png"},
        {id: "fruit_orange_s", src:"assets/game_fruit_orange_s.png"},
        {id: "fruit_purple", src:"assets/game_fruit_purple.png"},
        {id: "fruit_purple_l", src:"assets/game_fruit_purple_l.png"},
        {id: "fruit_purple_r", src:"assets/game_fruit_purple_r.png"},
        {id: "fruit_purple_s", src:"assets/game_fruit_purple_s.png"},
        {id: "fruit_red", src:"assets/game_fruit_red.png"},
        {id: "fruit_red_l", src:"assets/game_fruit_red_l.png"},
        {id: "fruit_red_r", src:"assets/game_fruit_red_r.png"},
        {id: "fruit_red_s", src:"assets/game_fruit_red_s.png"},
        {id: "fruit_yellow", src:"assets/game_fruit_yellow.png"},
        {id: "fruit_yellow_l", src:"assets/game_fruit_yellow_l.png"},
        {id: "fruit_yellow_r", src:"assets/game_fruit_yellow_r.png"},
        {id: "fruit_yellow_s", src:"assets/game_fruit_yellow_s.png"}
    ];

    let onAssetsLoadComplete = function() {
        onLoadCompleteListener && onLoadCompleteListener();
        let background = new createjs.Shape();
        let gameBg = assetsLoader.getResult("game_bg");
        if(gameBg.width > width) {
            scale = (width / gameBg.width) .toFixed(2)
        }
        background.graphics.beginBitmapFill(gameBg)
            .drawRect(0,0, width, height);
        background.cache(0, 0, width, height);

        stage.addChild(background);

        initIntro();
    };

    let initIntro = function() {
        let introScreen = new IntroScreen({
            x:0, y:0, width: width, height: height
        }, assetsLoader, scale);
        introScreen.addActionOnGameStart(function() {
            stage.removeChild(introScreen.getContainer());
            initGameStage();
        }, this);
        stage.addChild(introScreen.getContainer());
        stage.update();
    };

    let initGameStage = function() {
        stage.clear();

        let gameScreen = new GameScreen({
            x:0, y:0, width: width, height: height
        }, assetsLoader, scale);

        gameScreen.addActionOnGameEnd(function () {
            stage.removeChild(gameScreen.getContainer());
            initResults(gameScreen.getTotalScope());
        }, this);

        stage.addChild(gameScreen.getContainer());
        gameScreen.start();
    };

    let initResults = function(scorePoints) {

        let resultScreen = new ResultScreen(scorePoints, {
            x:0, y:0, width: width, height: height
        }, assetsLoader, scale);

        stage.addChild(resultScreen.getContainer());
        stage.update();
    };

    let tick = function (event) {
        stage.update(event);
    };

    let init = function() {
        createjs.MotionGuidePlugin.install();
        stage = new  createjs.Stage(canvasElementId);
        createjs.Touch.enable(stage);
        createjs.Sound.alternateExtensions = ["mp3"];

        if(!width && width !== 0) {
            stage.canvas.width = width;
        }
        if(!height && height !== 0) {
            stage.canvas.height= height;
        }

        assetsLoader = new createjs.LoadQueue(false);
        assetsLoader.addEventListener("complete", onAssetsLoadComplete);
        assetsLoader.installPlugin(createjs.Sound);
        assetsLoader.loadManifest(assetsManifest);

        createjs.Ticker.framerate = 40;
        createjs.Ticker.addEventListener("tick", tick);

    };

    self.init = function () {
        init();
    };

    self.onLoadCompleate = function (listener) {
        onLoadCompleteListener = listener;
    }
}