"use strict";

function IntroScreen(bounds, assetsLoader, scale) {
    let self = this;
    let gameListener = {
        listener: null,
        context: null
    };

    let container = new createjs.Container()
        .set({
            x: bounds.x,
            y: bounds.y,
            width: bounds.width,
            height: bounds.height
        });

    let btnPlay = new createjs.Bitmap(assetsLoader.getResult("play_btn"))
        .set({scale: scale});
    let btnPlayBounds = btnPlay.getTransformedBounds();

    let btnContainer = new createjs.Container()
        .set({
            x: bounds.width/2 - btnPlayBounds.width/2,
            y: bounds.height/2 - btnPlayBounds.height/2,
        });

    btnContainer.addChild(btnPlay);
    container.addChild(btnContainer);
    btnContainer.addEventListener("click", function() {
        if(gameListener.listener) {
            gameListener.listener.call(gameListener.context);
        }
    });

    self.getContainer = function() {
        return container;
    };

    self.addActionOnGameStart = function(listener, ctx) {
        gameListener.listener = listener;
        gameListener.context = ctx;
    };
}